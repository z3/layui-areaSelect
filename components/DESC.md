#项目代码目录说明

### port-app
基于nodejs-express的app服务端api接口项目

### port-admin
基于nodejs-express的管理后台api接口项目

### spa-admin
基于layui admin的后台管理UI SPA项目

### app-member
基于uniapp的用户端app项目